using UnityEngine;

namespace Homework.Barking
{
    public class Wail: IBarkable
    {
        public void Bark()
        {
           Debug.Log($"Ayyyyy... Ay-Ay-Ayyyyy(((");
        }
    }
}