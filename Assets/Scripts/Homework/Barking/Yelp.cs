using UnityEngine;

namespace Homework.Barking
{
    public class Yelp: IBarkable
    {
        public void Bark()
        {
            Debug.Log($"Гав-Гав-Гав-Гав!!! Гав-Ав!)))");
        }
    }
}