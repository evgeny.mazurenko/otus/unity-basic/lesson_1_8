namespace Homework.Barking
{
    public interface IBarkable
    {
        void Bark();
    }
}