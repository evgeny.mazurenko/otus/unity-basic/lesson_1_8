using UnityEngine;

namespace Homework.Common
{
    public class CreationTimeOfGameObject
    {
        public CreationTimeOfGameObject(GameObject gameObject)
        {
            GameObject = gameObject;
            CreationTime = Time.time;
        }
        
        public float CreationTime { get; }
        
        public GameObject GameObject { get; }
    }
}