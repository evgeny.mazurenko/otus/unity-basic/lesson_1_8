using UnityEngine;

namespace Homework.Movement
{
    /**
     * TODO:
     * 1. Реализовать этот тип перемещения по аналогии с Walk, но отличающийся от него,
     * например, пусть перемещение будет по окружности заданного радиуса.
     * 2. Заменить передвижение у HappyDog и/или SadDog этим типом. Убедиться, что он работает.
     */
    
    /**
     * Детали решения:
     * 1. Перемещение реализовано по окружности по дуге от Pi/2 (12 часов) до 3Pi/2 (6 часов) и обратно.
     *    По достижении границ происходит смена направления движения.
     * 2. HappyDog имеет реализацию движения Walk, SadDog - Run.
     */
    public class Run : Move
    {
        private readonly float _radius;
        private float _alpha;

        public Run(MonoBehaviour owner, float speed, float radius) : base(owner, speed)
        {
            _radius = radius;
            var random = new System.Random();
            //Первоначальная фаза смещения
            _alpha = (float)(Mathf.PI * random.NextDouble() + Mathf.PI / 2);
        }

        public override void Execute()
        {
            float newAlpha = (_alpha + Direction * Time.deltaTime * _speed) % (2 * Mathf.PI);

            if (newAlpha < Mathf.PI / 2)
            {
                Direction = -1 * Direction;
                newAlpha = Mathf.PI / 2;
            }

            if (newAlpha > 3 * Mathf.PI / 2)
            {
                Direction = -1 * Direction;
                newAlpha = 3 * Mathf.PI / 2;
            }

            _alpha = newAlpha;
            ChangePosition();
        }

        private void ChangePosition()
        {
            var newPosition = Owner.transform.position;
            newPosition.x = Mathf.Cos(_alpha) * _radius;
            newPosition.y = Mathf.Sin(_alpha) * _radius;
            Owner.transform.position = newPosition;
        }
    }
}