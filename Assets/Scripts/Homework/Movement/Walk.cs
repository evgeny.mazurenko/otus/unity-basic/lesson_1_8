using UnityEngine;

namespace Homework.Movement
{
    public class Walk : Move
    {
        private readonly float _minX;
        private readonly float _maxX;
        
        
        public Walk(MonoBehaviour owner, float speed, float minX, float maxX) : base(owner, speed)
        {
            _minX = minX;
            _maxX = maxX;
        }

        public override void Execute()
        {
            var newPosition = Owner.transform.position;
            newPosition.x += Direction * Time.deltaTime * _speed;

            Owner.transform.position = newPosition;

            if (newPosition.x > _maxX)
                Direction = -1;
            else if (newPosition.x < _minX)
                Direction = 1;
        }
    }
}