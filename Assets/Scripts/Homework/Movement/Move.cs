using System;
using UnityEngine;

namespace Homework.Movement
{
    public abstract class Move
    {
        private int _direction;
        private int _oldDirection;
        
        protected readonly float _speed;
        protected readonly MonoBehaviour Owner;
        protected int Direction
        {
            get => _direction;
            set
            {
                _oldDirection = _direction;
                _direction = value;
                if (_direction != _oldDirection)
                {
                    OnChangeDirection?.Invoke();
                }
            }
        }

        protected Move(MonoBehaviour owner, float speed)
        {
            Owner = owner;
            _speed = speed;
            _direction = 1;
            _oldDirection = _direction;
        }
        
        public event Action OnChangeDirection;

        public abstract void Execute();
    }
}