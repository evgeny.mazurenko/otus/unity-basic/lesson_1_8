using Homework.Barking;
using Homework.Common;
using Homework.Movement;
using UnityEngine;

namespace Homework.Dogs
{
    /**
     * TODO:
     * 1. Добавить всем собакам способность гавкать: достаточно метода, пишущего в Unity-консоль строку с сообщение.
     * 2. HappyDog должен гавкать более радостно.
     * 3. (сложно) Пусть собаки гавкают только тогда, когда меняют направление движения.
     */
    
    /**
     * Детали решения:
     * 1. Лай реализован через композицию элемента типа Homework.Barking.IBarkable, инициализируемого в конструкторе потомков класса Dog.
     * 2. Особенности лая приведены в методах Bark реализаций интерфейса Wail и Yelp.
     * 3. Лай при смене направления реализован через подписку метода IBarkable.Bark на событие Move.OnChangeDirection.
     */
    public abstract class Dog : MonoBehaviour, IColorChangeable
    {
        protected Move Move;
        protected IBarkable Barkable;
        
        private SpriteRenderer _spriteRenderer;

        protected void Start()
        {
            InputController.Instance.OnColorChanged += ChangeColor;
            if (IsDogReady())
            {
                Move.OnChangeDirection += Barkable.Bark;
            }
        }

        protected void Update()
        {
            Move.Execute();
        }

        private void OnDestroy()
        {
            InputController.Instance.OnColorChanged -= ChangeColor;
            if (IsDogReady())
            {
                Move.OnChangeDirection -= Barkable.Bark;
            }
        }

        private bool IsDogReady()
        {
            return Move != null && Barkable != null;
        }
        
        protected SpriteRenderer GetSpriteRenderer()
        {
            if (_spriteRenderer == null)
                _spriteRenderer = GetComponentInChildren<SpriteRenderer>();

            return _spriteRenderer;
        }
        
        public void ChangeColor()
        {
            var random = new System.Random();
            var colorComponent = (float)random.NextDouble();
            ChangeColor(colorComponent);
        }

        protected abstract void ChangeColor(float colorComponent);
    }
}