using Homework.Barking;
using Homework.Movement;
using UnityEngine;

namespace Homework.Dogs
{
    public class HappyDog : Dog
    {
        public HappyDog()
        {
            Move = new Walk(this, 1,-4, 4);
            Barkable = new Yelp();
        }
        protected override void ChangeColor(float colorComponent)
        {
            GetSpriteRenderer().color = new Color(0.5f + colorComponent / 2, 0.1f, 0.1f);
        }
    }
}