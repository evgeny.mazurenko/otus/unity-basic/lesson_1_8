using System.Collections;
using System.Collections.Generic;
using Homework.Common;
using UnityEngine;

namespace Homework
{
    /**
     * TODO:
     * 1. Найти примеры полиморфизма в уже написанном коде и в том, что будет написан вами.
     * 2. Реализовать удаление объектов из коллекции _spawnedObjects.
     * 3. Заменить тип коллекции на более подходящий к данному случаю. Объяснить, если замена не требуется.
     */
    
    /**
     * Детали решения:
     * 1. Примеры полиморфизма:
     *    1.1. Перегрузка методов:
     *         - Homework.Spawner.SpawnNext:
     *           * Метод System.Random.Next для типа int имеет дополнительные перегрузки для возврата как любого допустимого целого числа,
     *             так и для числа, ограниченного сверху и снизу.
     *    1.2. Подтипы общего класса/интерфейса:
     *         - Homework.Spawner.RemoveFirstDog
     *           * Метод UnityEngine.Object.Destroy принимает аргумент типа Object, но передается его наследник GameObject.
     *    1.3. Параметризация обобщений:
     *         - Homework.Spawner.SpawnNext:
     *           * Первым аргументом метода UnityEngine.Object.Instantiate типа T, являющегося наследником класса Object, передается переменная типа GameObject.
     *           * Метод System.Collections.Generic.LinkedList[T].AddLast принимает аргумент типа T, указаваемого при инициализации коллекции, в данном случае CreationTimeOfGameObject.
     *    * Для прочих классов подобное исследование пропускаю.
     * 2. Удаление из коллекции _spawnedObjects реализовано через проверку максимального время жизни собаки (параметр _dogLifeTime).
     *    Время создания элемента коллекции хранится в классе-обертке Homework.Common.CreationTimeOfGameObject.
     *    В случае превышения порогового значения первого элемента коллекции сначала вызывается метод Destroy для вложенного GameObject,
     *    а затем удаляется сам элемент из коллекции.
     * 3. Тип коллекции заменен на LinkedList, так как из операций доступа требуется только знать размер коллекций, чтение/удаление первого элемента и добавление в конец.
     *    Для этих целей оптимально подходит данная коллекция, так как не содержит в своей реализации пересоздаваемый массив
     *    и имеет закэшированные значения размерности и граничных элеменов, тем самым гарантируя стабильную минимальную алгоритмическую сложность.
     */
    public class Spawner : MonoBehaviour
    {
        [SerializeField]
        private int _totalAmount;

        [SerializeField]
        private float _spawnDelay;

        [SerializeField] 
        private float _dogLifeTime;

        [SerializeField]
        private List<GameObject> _objectsToSpawn;

        private readonly LinkedList<CreationTimeOfGameObject> _spawnedObjects = new LinkedList<CreationTimeOfGameObject>();


        void Start()
        {
            StartCoroutine(SpawnNext());
        }

        private IEnumerator SpawnNext()
        {
            var random = new System.Random();
            int i;

            while (true)
            {
                yield return new WaitForSeconds(_spawnDelay);
                
                RemoveFirstDog();
                
                if (_spawnedObjects.Count < _totalAmount)
                {
                    i = random.Next(_objectsToSpawn.Count);

                    var spawnedObject = Instantiate(_objectsToSpawn[i], transform);

                    _spawnedObjects.AddLast(new CreationTimeOfGameObject(spawnedObject));
                }
            }
        }

        private void RemoveFirstDog()
        {
            if (_spawnedObjects.Count == 0)
                return;
            
            CreationTimeOfGameObject firstDog = _spawnedObjects.First.Value;
            if (Time.time - firstDog.CreationTime > _dogLifeTime)
            {
                Destroy(firstDog.GameObject);
                _spawnedObjects.RemoveFirst();
            }
        }
    }
}